package com.company;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class XMLConfigManager implements ConfigManager {

    XStream xStream;
    FileManager fileManager;

    public XMLConfigManager(String fileName) {
        fileManager = new FileManager(fileName);
        xStream = new XStream(new StaxDriver());
    }

    public void registerAliases(RegistrableConfig registrableConfig) {
        HashMap<String, Class> aliases = registrableConfig.getAliases();
        for(Map.Entry<String, Class> mapEntry : aliases.entrySet()) {
            xStream.alias(mapEntry.getKey(), mapEntry.getValue());
        }
    }

    @Override
    public Config getConfig() throws ConfigManagerException, InvalidConfigException {
        String xmlString = null;
        try {
            xmlString = fileManager.readFromFile();
        }
        catch (IOException e) {
            throw new ConfigManagerException("Unable to read XML config file:\n" + e.getMessage());
        }

        assert xmlString != null;
        Config config = null;

        try {
            config = (Config)xStream.fromXML(xmlString);
        }
        catch (XStreamException e) {
            throw new ConfigManagerException("Unable to parse XML config file:\n" + e.getMessage());
        }

        config.validateConfig();

        return config;
    }

    @Override
    public void saveConfig(Config config) throws ConfigManagerException {
        String xmlString = xStream.toXML(config);

        try {
            fileManager.saveToFile(xmlString);
        }
        catch (IOException e) {
            throw new ConfigManagerException("Unable to save config file.");
        }
    }
}
