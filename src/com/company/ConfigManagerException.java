package com.company;

public class ConfigManagerException extends Exception {

    public ConfigManagerException(String message) {
        super(message);
    }
}
