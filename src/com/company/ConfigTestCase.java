package com.company;

public class ConfigTestCase {

    public void run() {
        // Test use case.

        Config config = null;
        XMLConfigManager xmlConfigManager = new XMLConfigManager("config.xml");
        xmlConfigManager.registerAliases(new Config());

        try {
            config = xmlConfigManager.getConfig();
            System.out.println("Config read.");
        } catch (ConfigManagerException | InvalidConfigException e) {
            System.out.println(e.getMessage() + "\nUsing initial config.");
            config = new Config();
        }

        System.out.println(config.toString());

        try {
            xmlConfigManager.saveConfig(config);
            System.out.println("Config saved.");
        } catch (ConfigManagerException e) {
            e.printStackTrace();
        }
    }
}
