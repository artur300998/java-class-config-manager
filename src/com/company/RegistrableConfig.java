package com.company;

import java.util.HashMap;

public interface RegistrableConfig {
    HashMap<String, Class> getAliases();
}
