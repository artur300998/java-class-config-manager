package com.company;

public class InvalidConfigException extends Exception {

    public InvalidConfigException(String message) {
        super(message);
    }
}
