package com.company;

public interface ConfigManager {
    Config getConfig() throws ConfigManagerException, InvalidConfigException;
    void saveConfig(Config config) throws ConfigManagerException;
}
