package com.company;

import java.io.Serializable;
import java.util.HashMap;

public class Config implements Serializable, RegistrableConfig {

    private Counter totalCounter, userCounter1, userCounter2;

    public Config() {
        this.totalCounter = new Counter();
        this.userCounter1 = new Counter();
        this.userCounter2 = new Counter();
    }

    public Config(long totalCounter, long userCounter1, long userCounter2) throws InvalidConfigException {
        try {
            this.totalCounter = new Counter(totalCounter);
            this.userCounter1 = new Counter(userCounter1);
            this.userCounter2 = new Counter(userCounter2);
        }
        catch (CounterException e) {
            throw new InvalidConfigException("Config is invalid:\n" + e.getMessage());
        }
    }

    public Counter getTotalCounter() {
        return totalCounter;
    }

    public Counter getUserCounter1() {
        return userCounter1;
    }

    public Counter getUserCounter2() {
        return userCounter2;
    }

    public void validateConfig() throws InvalidConfigException {
        try {
            totalCounter.validateCounter();
            userCounter1.validateCounter();
            userCounter2.validateCounter();
        }
        catch (CounterException e) {
            throw new InvalidConfigException("Config is invalid:\n" + e.getMessage());
        }
    }

    @Override
    public String toString() {
        return "Config{" +
                "totalCounter=" + totalCounter.getCounter() +
                ", userCounter1=" + userCounter1.getCounter() +
                ", userCounter2=" + userCounter2.getCounter() +
                '}';
    }

    @Override
    public HashMap<String, Class> getAliases() {
        HashMap<String, Class> aliases = new HashMap<>();

        aliases.put(Config.class.getSimpleName(), Config.class);
        aliases.put(Counter.class.getSimpleName(), Counter.class);

        return aliases;
    }
}
