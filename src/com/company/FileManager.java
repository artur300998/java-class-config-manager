package com.company;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileManager extends File {

    public FileManager(String fileName) {
        super(fileName);
    }

    public void saveToFile(String xmlString) throws IOException {
        try (FileWriter fileWriter = new FileWriter(this)) {
            fileWriter.write(xmlString);
        }
    }

    public String readFromFile() throws IOException {
        char[] fileBuffer;

        FileReader fileReader = null;
        try {
            fileReader = new FileReader(this);
            fileBuffer = new char[(int) length()];
            fileReader.read(fileBuffer);
        }
        finally {
            if (fileReader != null) {
                fileReader.close();
            }
        }

        return new String(fileBuffer);
    }
}
