package com.company;

import java.io.Serializable;

public class Counter implements Serializable, Comparable<Counter> {

    private long counter;

    public Counter() {
        this.counter = 0;
    }

    public Counter(long value) throws CounterException {
        if (value < 0)
            throw new CounterException("Counter less that zero on initialization.");
        this.counter = value;
    }

    public long getCounter() {
        return counter;
    }

    public void validateCounter() throws CounterException {
        if (counter < 0)
            throw new CounterException("Counter less that zero on validation.");
    }

    public void add(long value) throws CounterException {
        if (counter + value < 0)
            throw new CounterException("Counter overflow on addition.");

        counter += value;
    }

    @Override
    public int compareTo(Counter o) {
        return Long.compare(counter, o.counter);
    }
}
